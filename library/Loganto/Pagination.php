<?php
/**
 * @namespace
 */
namespace Loganto;
use Pagination\Exception\RuntimeException;

class Pagination
{
    /*
     * Items per page
     */
	private $perPage = 25;

    /*
     * Max buttons to the right of current
     */
    private $buttonsAfter = 5;

    /*
     * Max buttons to the left of current
     */
    private $buttonsBefore = 5;

    /*
     * If supplied page goes beyond the maximum number of pages - set
     * page to the last one
     */
    private $fixPageNumbers = false;

    /*
     * Url before a page number
     */
    private $prefix = '';

    /*
     * Url after a page number
     */
    private $postfix = '';

    /*
     * Should we add page numbers to urls? (data-page3 is always set)
     */
    private $addPageNumbers = true;

    /*
     * ul css class
     */
    private $class = 'loganto_pagination';

    /*
     * Should we render these buttons
     */
	private $previousAndNext = true;

    /*
     * Should we add page numbers to urls and attr-data
     */
	private $firstAndLast = true;

    /*
     * Fillers (translate yourself or exchange with icons)
     */
	private $fillers =  array(
		'first' => 'First',
		'previous' => 'Previous',
		'next' => 'Next',
		'last' => 'Last'
	);

    function __construct() 
    {}


    /*
     * Dumb getters and setters
     */
    public function setPerPage($type)
    {
        $this->perPage = (int)$type;
        return $this;
    }
    public function getPerPage()
    {
        return $this->perPage;
    }


    public function setButtonsAfter( $b )
    {
        $this->buttonsAfter = (int)$b;
        return $this;
    }
    public function getButtonsAfter()
    {
        return $this->buttonsAfter;
    }


    public function setButtonsBefore($b)
    {
        $this->buttonsBefore = (int)$b;
        return $this;
    }
    public function getButtonsBefore()
    {
        return $this->buttonsBefore;
    }


    public function setFixPage($b)
    {
        $this->fixPageNumbers = (bool)$b;
        return $this;
    }
    public function getFixPage() 
    {
        return $this->fixPageNumbers;
    }


    public function setPaginationPrefix($p)
    {
        $this->prefix = (string)$p;
        return $this;
    }
    public function getPaginationPrefix() 
    {
        return $this->prefix;
    }


    public function setPaginationPostfix($p)
    {
        $this->postfix = (string)$p;
        return $this;
    }
    public function getPaginationPostfix()
    {
        return $this->postfix;
    }



    public function setAddPageNumbers($a)
    {
        $this->addPageNumbers = (bool)$a;
        return $this;
    }
    public function getAddPageNumbers() 
    {
        return $this->addPageNumbers;
    }


    public function setClass($c)
    {
        $this->class = (string)$c;
        return $this;
    }
    public function getClass()
    {
        return $this->class;
    }


    public function setPreviousAndNext($p)
    {
        $this->previousAndNext = (bool)$p;
        return $this;
    }
    public function getPreviousAndNext()
    {
        return $this->previousAndNext;
    }


    public function setFirstAndLast($p)
    {
        $this->firstAndLast = (bool)$p;
        return $this;
    }
    public function getFirstAndLast() 
    {
        return $this->firstAndLast;
    }


    public function setFillers(array $f) 
    {
        $this->fillers = $f;
        return $this;
    }
    public function getFillers() 
    {
        return $this->fillers;
    }



    /*
     * Actual generic skeleton generator
     */
    public function build($page, $count) 
    {
		$totalPages = ($this->perPage===0) ? 0 : (int)ceil($count / $this->perPage);
		if (($page > $totalPages) && $this->fixPageNumbers) {
			$page = $totalPages;
		}
		
		$nowStart = $page * $this->perPage - $this->perPage;
		$nowFinish = ($page !== $totalPages) ? $page * $this->perPage - 1 : $totalPages - 1;
		$pagination = '<ul class = "' . $this->class . '">';

		/*
		 * First two (Previous and First)
		 */
		if ( $page > 1 ) {
			if ($this->firstAndLast) {
                $p = $this->addPageNumbers ? 1 : '';
				$pagination .= '<li class = "first"><a data-page = "1" href = "' . $this->prefix . $p . $this->postfix . '">' . $this->fillers['first'] . '</a></li>';
			}
			if ($this->previousAndNext) {
                $p = $this->addPageNumbers ? ($page - 1) : '';
				$pagination .= '<li class = "previous"><a data-page = "' . ($page - 1) . '" href = "' . $this->prefix . $p . $this->postfix . '">' . $this->fillers['previous'] . '</a></li>';
			}
		} else {
			if ($this->firstAndLast) {
				$pagination .= '<li class = "first disabled"><a data-page = "" href = "javascript: void(0)">' . $this->fillers['first'] . '</a></li>';
			}
			if ($this->previousAndNext) {
				$pagination .= '<li class = "previous disabled"><a data-page = "" href = "javascript: void(0)">' . $this->fillers['previous'] . '</a></li>';
			}
		}

		/*
		 * Buttons before (I'm using $this->perPage maximum)
		 */
		$counter = $page - $this->buttonsBefore;
		$counter = ($counter < 1) ? 1 : $counter;
		while ($counter < $page) {
            $p = $this->addPageNumbers ? $counter : '';
			$pagination .= '<li><a data-page = "' . $counter . '" href = "' . $this->prefix . $p . $this->postfix . '">' . $counter . '</a></li>';
			$counter++;
		}

		/*
		 * This button
		 */
        $p = $this->addPageNumbers ? $page : '';
		$pagination .= '<li class = "active"><a data-page="' . $page . '" href = "javascript: void(0)">' . $page . '</a></li>';

		/*
		 * Buttons after (I'm using $this->perPage maximum)
		 */
		$counter = $page + 1;
		while ($counter <= $totalPages && $counter <= ($page + $this->buttonsAfter)) {
            $p = $this->addPageNumbers ? $counter : '';
			$pagination .= '<li><a data-page = "' . $counter . '" href = "' . $this->prefix . $p . $this->postfix . '">' . $counter . '</a></li>';
			$counter++;
		}

		/*
		 * Last two (Next and Last)
		 */
		if (($page + 1) <= $totalPages) {
			if ($this->previousAndNext) {
                $p = $this->addPageNumbers ? ($page + 1) : '';
				$pagination .= '<li class = "next"><a data-page="' . ($page + 1) . '" href = "' . $this->prefix . $p . $this->postfix . '">' . $this->fillers['next'] . '</a></li>';
			}
			if ($this->firstAndLast) {
                $p = $this->addPageNumbers ? $totalPages : '';
				$pagination .= '<li class = "last"><a data-page="' . $totalPages . '" href = "' . $this->prefix . $p . $this->postfix . '">' . $this->fillers['last'] . '</a></li>';
			}
		} else {
			if ($this->previousAndNext) {
				$pagination .= '<li class = "next disabled"><a data-page = "" href = "javascript: void(0)">' . $this->fillers['next'] . '</a></li>';
			}
			if ($this->firstAndLast) {
				$pagination .= '<li class = "last disabled"><a data-page = "" href = "javascript: void(0)">' . $this->fillers['last'] . '</a></li>';
			}
		}

		return $pagination . "</ul>";
	}
}
