<?php

namespace Loganto\Pagination\Exception;

class RuntimeException extends \RuntimeException implements \Loganto\Pagination\Exception\ExceptionInterface
{}
